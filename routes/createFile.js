const { Router } = require('express');
const router = Router();
const path = require('path');
const fs = require('fs');

router.post('/files', (req, res) => {
    const { filename, content } = req.body;
    const extentionFiles = path.extname(filename);
    const allowedExtention = ['.log', '.txt', '.json', '.yaml', '.xml'];
    if (!content) {
        return res
            .status(400)
            .json({ message: "Please specify 'content' parameter" });
    }
    if (!filename) {
        return res
            .status(400)
            .json({ message: "Please specify 'content' parameter" });
    }
    if (allowedExtention.includes(extentionFiles)) {
        fs.writeFile(
            path.join(__dirname, '../files', filename),
            content,
            (err) => {
                if (err) {
                    return res.status(500).json({ message: 'Server error' });
                } else {
                    return res.status(200).json({
                        message: 'File created successfully',
                    });
                }
            }
        );
    } else {
        return res.status(400).json({
            message: `Thе extention ${extentionFiles} doesn't supported`,
        });
    }
});

module.exports = router;
