const { Router } = require('express');
const router = Router();
const path = require('path');
const fs = require('fs');

router.get('/files', (_, res) => {
    fs.readdir(path.join(__dirname, '..', '/files'), (err, files) => {
        if (err) {
            return res.status(500).json({ message: 'Server error' });
        }
        if (files.length === 0) {
            return res.status(400).json({ message: 'Client error' });
        }
        return res.status(200).json({ message: 'Success', files });
    });
});

module.exports = router;
