const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();
const cors = require('cors');

const createFile = require('./routes/createFile');
const getFiles = require('./routes/getFiles');
const getFile = require('./routes/getFile');

app.use(cors());
app.use(express.json());

app.use('/api', createFile);
app.use('/api', getFiles);
app.use('/api', getFile);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`);
});
